import os
import nox  # type: ignore

os.environ.update({"PDM_IGNORE_SAVED_PYTHON": "1"})

PYTHON_DEFAULT_VERSION = "3.10"


@nox.session(python=PYTHON_DEFAULT_VERSION)
def lint(session):
    session.run("echo", "done", external=True)


@nox.session(python=PYTHON_DEFAULT_VERSION)
def test(session):
    session.run("pdm", "run", "mkdocs", "build", "--clean", external=True)
    session.run("rm", "-Rf", "site", external=True)


@nox.session(python=PYTHON_DEFAULT_VERSION)
def build(session):
    session.run("pdm", "run", "mkdocs", "build", "--site-dir", 'public', external=True)

.PHONY: help docker-run run

IMAGE_NAME = wedding
IMAGE_TAG = dev
MANAGER = pdm run

PY_IMAGE ?= python
PY_IMAGE_VERSION ?= 3.10-slim
PORT ?= 9500

all: run # Default command, runs service

run: # Run service
	$(MANAGER) mkdocs serve -a 0.0.0.0:$(PORT) --no-livereload

docker-run: # Build and run service in Docker
	docker build \
		--build-arg PY_IMAGE=$(PY_IMAGE) \
		--build-arg PY_IMAGE_VERSION=$(PY_IMAGE_VERSION) \
		-t $(IMAGE_NAME):$(IMAGE_TAG) \
		-f ./Dockerfile \
		.
	docker run --rm -it \
		-p $(PORT):$(PORT) \
		$(IMAGE_NAME):$(IMAGE_TAG)

help: # Show help for each of the Makefile recipes.
	@grep -E '^[a-zA-Z0-9 -]+:.*#'  Makefile | \
	sort | \
	while read -r l; \
	do printf "\033[1;32m$$(echo $$l | cut -f 1 -d':')\033[00m:$$(echo $$l | cut -f 2- -d'#')\n"; \
	done
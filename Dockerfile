ARG PY_IMAGE
ARG PY_IMAGE_VERSION

FROM ${PY_IMAGE}:${PY_IMAGE_VERSION}

ENV TZ=Europe/Moscow
ARG PDM_VERSION=2.10.0
ARG DEBIAN_FRONTEND=noninteractive

ENV PATH="/root/.local/bin:${PATH}"

RUN apt-get update && \
    apt-get install -y make g++ curl && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade --no-cache pip && \
    curl -sSL https://raw.githubusercontent.com/pdm-project/pdm/main/install-pdm.py | python3 - -v $PDM_VERSION && \
    echo "export PATH=/root/.local/bin:${PATH}" >> $HOME/.profile

WORKDIR /app

COPY ["./pdm.lock", "./pyproject.toml", "./"]

RUN pdm install --production --global --project /app --check --verbose && \
    pdm cache clear

COPY . .

CMD ["make"]

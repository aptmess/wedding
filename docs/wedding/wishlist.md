# Что мы хотели бы в подарок на свадьбу

[Ссылка вишлист](#){ .md-button }

![Image title](https://dummyimage.com/600x400/f5f5f5/aaaaaa#only-light)
![Image title](https://dummyimage.com/600x400/21222c/d5d7e2#only-dark)

## Grid

<div class="grid cards" markdown>

- :fontawesome-brands-html5: __HTML__ for content and structure
- :fontawesome-brands-js: __JavaScript__ for interactivity
- :fontawesome-brands-css3: __CSS__ for text running out of boxes
- :fontawesome-brands-internet-explorer: __Internet Explorer__ ... huh?

</div>

::timeline::

- content: First implementation.
  icon: ':material-rocket-launch-outline:'
  sub_title: 2022-Q1
  title: Launch
- content: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  icon: ':fontawesome-solid-gem:'
  sub_title: 2022-Q2
  title: New features
- content: Lorem ipsum dolor sit amet.
  icon: ':material-gauge-empty:'
  sub_title: 2022-Q3
  title: More features!
- content: Lorem ipsum dolor sit amet.
  icon: ':material-bug:'
  sub_title: 2022-Q4
  title: Bugs!

::/timeline::

[//]: # (::cards::)

[//]: # ()
[//]: # ([)

[//]: # (  {)

[//]: # (    "title": "Zeus",)

[//]: # (    "content": "Lorem ipsum dolor sit amet.",)

[//]: # (    "image": "https://icon-library.com/images/0b1450ee9e.png")

[//]: # (  },)

[//]: # (  {)

[//]: # (    "title": "Athena",)

[//]: # (    "content": "Lorem ipsum dolor sit amet.",)

[//]: # (    "image": "./img/icons/003-athena.png")

[//]: # (  },)

[//]: # (  {)

[//]: # (    "title": "Poseidon",)

[//]: # (    "content": "Lorem ipsum dolor sit amet.",)

[//]: # (    "image": "./img/icons/007-poseidon.png")

[//]: # (  },)

[//]: # (  {)

[//]: # (    "title": "Artemis",)

[//]: # (    "content": "Lorem ipsum dolor sit amet.",)

[//]: # (    "image": "./img/icons/021-artemis.png")

[//]: # (  },)

[//]: # (  {)

[//]: # (    "title": "Ares",)

[//]: # (    "content": "Lorem ipsum dolor sit amet.",)

[//]: # (    "image": "./img/icons/006-ares.png")

[//]: # (  },)

[//]: # (  {)

[//]: # (    "title": "Nike",)

[//]: # (    "content": "Lorem ipsum dolor sit amet.",)

[//]: # (    "image": "./img/icons/027-nike.png")

[//]: # (  })

[//]: # (])

[//]: # ()
[//]: # (::/cards::)